# imports
import urllib2
from bs4 import BeautifulSoup
import HTMLParser
import time
import numpy as np
import scipy.stats

BASE = 'https://au.finance.yahoo.com'
CODE = 'BHP'
dates = []
opens = []
highs = []
lows = []
close = []
volume = []
adj_close = []


def main(soup):

    while soup.findAll('a', {'rel': 'next'}):
    
        link = soup.findAll('a', {'rel': 'next'})

        table = soup.findAll('table',{'class':'yfnc_datamodoutline1'})[0]

        rows = table.findChildren(['tr'])

        for row in rows:
            cells = row.findChildren(['td'])
            if len(cells) == 7:
                dates.append(str(cells[0].string))
                opens.append(float(cells[1].string))
                highs.append(float(cells[2].string))
                lows.append(float(cells[3].string))
                close.append(float(cells[4].string))
                volume.append(int(cells[5].string.replace(',','')))
                adj_close.append(float(cells[6].string))
        
        url = HTMLParser.HTMLParser().unescape(link[0]['href'])
        source = urllib2.urlopen(BASE + url).read()    
        soup = BeautifulSoup(source)
        time.sleep(1)
        
    opens_np = np.array(opens)
    print 'CODE::', CODE
    print len(dates), 'data points collected between', dates[0], 'and', dates[-1]
    print ''
    print '### Open Price ###'
    print 'Min:', opens_np.min()
    print 'Max:', opens_np.max()
    print '25%ile:', np.percentile(opens_np, 25)
    print '50%ile:', np.percentile(opens_np, 50)
    print '75%ile:', np.percentile(opens_np, 75)
    print 'Arithmetic Mean:', scipy.stats.tmean(opens_np)
    print 'Variance', np.var(opens_np)
    print 'Skewness', scipy.stats.skew(opens_np)
    print 'Excess Kurtosis:', scipy.stats.kurtosis(opens_np)


if __name__ == '__main__':
    start_time = time.time()
    source = urllib2.urlopen(BASE + '/q/hp?s=' + CODE).read()    
    soup = BeautifulSoup(source)
    main(soup)
    print time.time() - start_time, "s for program execution"
